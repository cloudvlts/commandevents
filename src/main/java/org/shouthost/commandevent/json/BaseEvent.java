package org.shouthost.commandevent.json;

import java.util.ArrayList;
import java.util.List;

public class BaseEvent {
	private String name;
	private int world = 0;
	private List<Location> location = new ArrayList<Location>();
	private String event = "interact";
	private List<Command> commands = new ArrayList<Command>();
	private boolean active = false;

	public int getWorld() {
		return world;
	}

	public void setWorld(int world) {
		this.world = world;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Location> getLocation() {
		return location;
	}

	public void setLocation(List<Location> location) {
		this.location = location;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public List<Command> getCommands() {
		return commands;
	}

	public void setCommands(List<Command> commands) {
		this.commands = commands;
	}
}
