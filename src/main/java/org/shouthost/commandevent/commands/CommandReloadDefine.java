package org.shouthost.commandevent.commands;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatMessageComponent;
import net.minecraft.util.EnumChatFormatting;
import org.shouthost.commandevent.core.CommandEvents;
import org.shouthost.commandevent.json.BaseEvent;
import org.shouthost.commandevent.utils.CoordRef;
import org.shouthost.commandevent.utils.EventParser;

import java.util.Map;

public class CommandReloadDefine extends CommandBase {
	@Override
	public String getCommandName() {
		return "reloaddefine";
	}

	@Override
	public String getCommandUsage(ICommandSender iCommandSender) {
		return "/reloaddefine name";
	}

	@Override
	public void processCommand(ICommandSender sender, String[] args) {
		if(args.length <= 0) throw new WrongUsageException(EnumChatFormatting.RED+getCommandUsage(sender));

		String name = args[0];
		if(name == null || name == "") throw new WrongUsageException(EnumChatFormatting.RED+getCommandUsage(sender));
		boolean unloaded = false;
		for(Map.Entry<CoordRef, BaseEvent> map : CommandEvents.eventList.entrySet()) {
			if(map.getValue().getName().equalsIgnoreCase(name)){
				CommandEvents.eventList.remove(map.getValue());
			}
		}
//		if(unloaded){
			if(EventParser.loadEvent(name)){
				((EntityPlayerMP) sender).addChatMessage(EnumChatFormatting.GREEN+"Event '"+name+"' reloaded");
				return;
			}else{
				((EntityPlayerMP) sender).addChatMessage(EnumChatFormatting.RED+"Event '"+name+"' cant be loaded");
				return;
			}
//		}
	}

	@Override
	public int compareTo(Object o) {
		return 0;
	}
}
