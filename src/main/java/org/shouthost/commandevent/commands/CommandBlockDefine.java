package org.shouthost.commandevent.commands;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.ChatMessageComponent;
import net.minecraft.util.EnumChatFormatting;
import org.shouthost.commandevent.core.CommandEvents;
import org.shouthost.commandevent.json.BaseEvent;
import org.shouthost.commandevent.json.Command;
import org.shouthost.commandevent.utils.EventParser;

import java.util.ArrayList;

public class CommandBlockDefine extends CommandBase {
	@Override
	public String getCommandName() {
		return "blockdefine";
	}

	@Override
	public String getCommandUsage(ICommandSender iCommandSender) {
		return "/blockdefine <name> <command>";
	}


	@Override
	public void processCommand(ICommandSender sender, String[] args) {
		if (args.length <= 0 || args.length <= 1)
			throw new WrongUsageException(EnumChatFormatting.RED + getCommandUsage(sender));
		String name = args[0];
		//perform check to see if anything as such exist in memory
		if(EventParser.doesEventExist(name)){
			((EntityPlayerMP) sender).addChatMessage(EnumChatFormatting.RED+"Event already exist!");
			return;
		}
		StringBuilder builder = new StringBuilder();
		for(int i = 1;i<args.length;i++) {
			builder.append(args[i]+" ");
		}
		ArrayList<Command> commandList = new ArrayList<Command>();
		Command command = new Command();
		command.setExecutor("console");
		command.setCommand(builder.toString());
		commandList.add(command);

		BaseEvent base = new BaseEvent();
		base.setName(name);
		base.setWorld(sender.getEntityWorld().provider.dimensionId);
		base.setEvent("interact");//default
		base.setCommands(commandList);
		CommandEvents.defineList.put((net.minecraft.entity.player.EntityPlayerMP) sender, base);
		((EntityPlayerMP) sender).addChatMessage(EnumChatFormatting.YELLOW+ "Right click on the block you wish to define with command");
	}

	@Override
	public int compareTo(Object o) {
		return 0;
	}
}
