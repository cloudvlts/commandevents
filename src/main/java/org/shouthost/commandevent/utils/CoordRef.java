package org.shouthost.commandevent.utils;

import net.minecraft.world.World;

public class CoordRef {
	final int world;
	final int x;
	final int y;
	final int z;

	public CoordRef(int world, int x, int y, int z) {
		this.world = world;
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public int getWorld() {
		return world;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getZ() {
		return z;
	}

	@Override
	public String toString() {
		return "world"+getWorld()+"x"+getX()+"y"+getY()+"z"+getZ();
	}
}
