package org.shouthost.commandevent.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.shouthost.commandevent.core.CommandEvents;
import org.shouthost.commandevent.json.BaseEvent;
import org.shouthost.commandevent.json.Location;

import java.io.*;
import java.util.Map;

public class EventParser {
	public static void saveEvent(BaseEvent event){
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(event);
		System.out.println(json);
		File file = new File(CommandEvents.base, event.getName()+".json");
		if (file.exists() && file.isFile()) file.delete();
		try {
			Writer writer = new OutputStreamWriter(new FileOutputStream(file), "UTF-8");
			writer.write(json);
			writer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static boolean doesEventExist(String name){
		for(Map.Entry<CoordRef, BaseEvent> map : CommandEvents.eventList.entrySet()) {
			if(map.getValue().getName().equalsIgnoreCase(name)){
				return true;
			}
		}
		return false;
	}

	public static boolean loadEvent(String name){
		File file = null;
		if(name.contains(".json")){
			file = new File(CommandEvents.base, name);
		}else{
			file = new File(CommandEvents.base, name+".json");
		}
		if (file == null || !file.exists()) return false;
		Gson gson = new Gson();
		BufferedReader br = null;
//		System.out.println("Preparing to load "+name);
		try {
			br = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		}
//		System.out.println("Loaded "+name);
		BaseEvent base = gson.fromJson(br, BaseEvent.class);
		CoordRef coords = new CoordRef(base.getWorld(), base.getLocation().get(0).getX(),base.getLocation().get(0).getY(), base.getLocation().get(0).getZ());
		CommandEvents.eventList.put(coords,base);
//		if(CommandEvents.eventList.containsKey(coords) && CommandEvents.eventList.containsValue(base))
//			System.out.println("Data is loaded in memory");
		try {
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

}
