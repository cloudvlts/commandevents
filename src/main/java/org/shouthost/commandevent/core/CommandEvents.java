package org.shouthost.commandevent.core;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.event.FMLServerStoppingEvent;
import net.minecraft.entity.player.EntityPlayerMP;
import org.shouthost.commandevent.commands.CommandBlockDefine;
import org.shouthost.commandevent.commands.CommandReloadDefine;
import org.shouthost.commandevent.events.PlayerEvents;
import org.shouthost.commandevent.json.BaseEvent;
import org.shouthost.commandevent.utils.CoordRef;
import org.shouthost.commandevent.utils.EventParser;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

@Mod(name = "CommandEvents", modid = "commandevent", version = "0.0.1")
public class CommandEvents {
	public static File base;
	public static HashMap<EntityPlayerMP,BaseEvent> defineList = new HashMap<EntityPlayerMP, BaseEvent>();
	public static HashMap<CoordRef,BaseEvent> eventList = new HashMap<CoordRef, BaseEvent>();
	public PlayerEvents ev;
	@Instance
	public CommandEvents instance;


	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		base = new File("Events");
		if(!base.exists()) base.mkdir();
	}

	@EventHandler
	public void init(FMLInitializationEvent event) {
		//TODO: load files into memory
		if(base.list().length > 0)
			for(String name : base.list()){
				if(EventParser.loadEvent(name)){
					System.out.println("Event '"+name+"' is loaded");
				}
			}
		ev = new PlayerEvents();
	}

	@EventHandler
	public void serverStarting(FMLServerStartingEvent event) {
		event.registerServerCommand(new CommandBlockDefine());
		event.registerServerCommand(new CommandReloadDefine());
	}

	@EventHandler
	public void serverStopping(FMLServerStoppingEvent event) {

	}
}
