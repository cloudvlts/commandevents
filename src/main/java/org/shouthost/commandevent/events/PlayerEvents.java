package org.shouthost.commandevent.events;

import net.minecraft.server.MinecraftServer;
import net.minecraft.util.EnumChatFormatting;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import org.shouthost.commandevent.core.CommandEvents;
import org.shouthost.commandevent.json.BaseEvent;
import org.shouthost.commandevent.json.Command;
import org.shouthost.commandevent.json.Location;
import org.shouthost.commandevent.utils.CoordRef;
import org.shouthost.commandevent.utils.EventParser;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

public class PlayerEvents {
	public PlayerEvents(){
		MinecraftForge.EVENT_BUS.register(this);
	}

	@ForgeSubscribe
	public void onPlayerInteract(PlayerInteractEvent event) {
		if(event.action == PlayerInteractEvent.Action.RIGHT_CLICK_BLOCK){
			if(CommandEvents.defineList.containsKey(event.entityPlayer)){
				event.setCanceled(true);
				BaseEvent base = CommandEvents.defineList.get(event.entityPlayer);
				if(!base.isActive()) {
					ArrayList<Location> list = new ArrayList<Location>();
					Location loc = new Location();
					loc.setX(event.x);
					loc.setY(event.y);
					loc.setZ(event.z);
					list.add(loc);
					base.setLocation(list);
					base.setActive(true);
					CoordRef coords = new CoordRef(event.entityPlayer.dimension, event.x, event.y, event.z);
					CommandEvents.eventList.put(coords, base);
					event.entityPlayer.addChatMessage(EnumChatFormatting.GREEN + "Block have been regisitered!");
					CommandEvents.defineList.remove(event.entityPlayer);
					EventParser.saveEvent(base);
					return;
				}
			}else{
				for(Map.Entry<CoordRef, BaseEvent> map : CommandEvents.eventList.entrySet()) {
					CoordRef coords = map.getKey();
					BaseEvent base = map.getValue();
					int world = coords.getWorld();
					int x = coords.getX();
					int y = coords.getY();
					int z = coords.getZ();
//					event.entityPlayer.addChatMessage(""+base.getEvent().equalsIgnoreCase("interact"));
//					event.entityPlayer.addChatMessage(""+world+" "+event.entityPlayer.dimension);
//					event.entityPlayer.addChatMessage(""+x+" "+event.x);
//					event.entityPlayer.addChatMessage(""+y+" "+event.y);
//					event.entityPlayer.addChatMessage(""+z+" "+event.z);
//					event.entityPlayer.addChatMessage(" ");
					if (base.getEvent().equalsIgnoreCase("interact") &&
						world == event.entityPlayer.dimension &&
					    x == event.x &&
						y == event.y &&
						z == event.z) {
							//TODO: Parse information from baseevent and have placeholder changed so events can carry through
							if(!base.getCommands().isEmpty()){
								event.setCanceled(true);
								for(Command command: base.getCommands()){
									String cmd = command.getCommand().replaceAll("!p", event.entityPlayer.getDisplayName());
									if(command.getExecutor().equals("console")) {
										String results = MinecraftServer.getServer().executeCommand(cmd);
									}else if(command.getExecutor().equals("player")){
										MinecraftServer.getServer().getCommandManager().executeCommand(event.entityPlayer, cmd);
									}
								}
							}
							return;
					}
				}
			}
		}
	}
}
